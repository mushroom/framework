<?php

namespace Mushroom;

use Mushroom\Container;
use Mushroom\Application;
use GuzzleHttp\ClientInterface;
use Mushroom\Contracts\Handler;
use Mushroom\Contracts\Pipeline;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler implements Handler
{
    protected array $processors = [];

    public function __construct(
        protected Pipeline $pipeline,
        protected ClientInterface $client
    ) {}

    public function handle(Request $request): Response
    {
        $response = $this->getResponse($request);

        return $this->pipeline
            ->send($response)
            ->through($this->processors)
            ->thenReturn();
    }

    protected function getResponse(Request $request): Response
    {
        $app = Container::resolve(Application::class);

        try {
            $response = $this->client->get($app->targetUrl() . $request->getRequestUri())->getBody()->getContents();
        } catch (BadResponseException $e) {
            $response = $e->getResponse()->getBody()->getContents();
        }

        return new Response($response);
    }
}
