<?php

namespace Mushroom;

use Closure;
use Throwable;
use RuntimeException;
use Mushroom\Container;
use Mushroom\Contracts\Processor;
use Symfony\Component\HttpFoundation\Response;
use Mushroom\Contracts\Pipeline as PipelineContract;

class Pipeline implements PipelineContract
{
    /**
     * The object being passed through the pipeline.
     *
     * @var mixed
     */
    protected $passable;

    /**
     * The array of class pipes.
     *
     * @var array
     */
    protected $pipes = [];

    /**
     * Set the object being sent through the pipeline.
     *
     * @param  mixed  $passable
     * @return $this
     */
    public function send($passable)
    {
        $this->passable = $passable;

        return $this;
    }

    /**
     * Set the array of pipes.
     *
     * @param  array|mixed  $pipes
     * @return $this
     */
    public function through($pipes)
    {
        $this->pipes = is_array($pipes) ? $pipes : func_get_args();

        return $this;
    }

    /**
     * Run the pipeline with a final destination callback.
     *
     * @param  \Closure  $destination
     * @return mixed
     */
    public function then(Closure $destination)
    {
        $pipeline = array_reduce(
            array_reverse($this->pipes()), $this->carry(), $destination
        );

        return $pipeline($this->passable);
    }

    /**
     * Run the pipeline and return the result.
     *
     * @return mixed
     */
    public function thenReturn()
    {
        return $this->then(function ($passable) {
            return $passable;
        });
    }

    /**
     * Get a Closure that represents a slice of the application onion.
     *
     * @return \Closure
     */
    protected function carry()
    {
        return function ($stack, string $pipe) {
            return function (Response $passable) use ($stack, $pipe) {
                $pipe = Container::resolve($pipe);

                if (!$pipe instanceof Processor) {
                    throw new RuntimeException(
                        sprintf("Processor '%s' must implement the '%s' interface", $pipe::class, Processor::class),
                        1
                    );
                }

                return $pipe->process($passable, $stack);
            };
        };
    }

    private function pipes(): array
    {
        return $this->pipes;
    }
}
