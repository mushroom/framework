<?php

namespace Mushroom;

use Closure;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;
use Mushroom\Exceptions\NotFoundException;
use Mushroom\Exceptions\ContainerException;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class Container implements ContainerInterface
{
    private array $services = [];

    private static ?ContainerInterface $instance = null;

    private function __construct() {}

    public static function getInstance(): ContainerInterface
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public static function resolve(string $id)
    {
        return static::getInstance()->get($id);
    }

    public static function bind(string $id, Closure $resolver)
    {
        static::getInstance()->set($id, $resolver);
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get(string $id)
    {
        if ($this->has($id)) {
            return $this->services[$id]($this);
        }

        return $this->autowire($id);
    }

    private function autowire(string $class)
    {
        if (!class_exists($class) && !interface_exists($class)) {
            throw new NotFoundException(sprintf("Service '%s' is not found", $class));
        }

        $reflector = $this->getReflector($class);

        if (!$reflector->isInstantiable()) {
            throw new ContainerException(sprintf("Service '%s' is not instantiable", $class));
        }

        if ($constructor = $reflector->getConstructor()) {
            return $reflector->newInstanceArgs(
                $this->getConstructorDependencies($constructor)
            );
        }

        return new $class();
    }

    private function getConstructorDependencies(ReflectionMethod $constructor): array
    {
        return array_map(function (ReflectionParameter $parameter) {
            return $this->resolveDependency($parameter);
        }, $constructor->getParameters());
    }

    private function resolveDependency(ReflectionParameter $dependency)
    {
        $type = $dependency->getType();

        if (is_null($type)) {
            throw new ContainerException(sprintf("Service '%s' cannot be resolved", $type->getName()));
        };

        try {
            return $this->get($type->getName());
        } catch (NotFoundException $e) {
            if (!$dependency->isDefaultValueAvailable()) {
                throw $e;
            }
        }

        return $dependency->getDefaultValue();
    }

    private function getReflector(string $class)
    {
        return new ReflectionClass($class);
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has(string $id): bool
    {
        return isset($this->services[$id]);
    }

    public function set(string $id, Closure $resolver)
    {
        $this->services[$id] = $resolver;
    }

    public function singleton(string $id, Closure $resolver)
    {
        $this->set($id, function () use ($resolver) {
            static $resolved;

            if (!$resolved) {
                $resolved = $resolver($this);
            }

            return $resolved;
        });
    }
}
