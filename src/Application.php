<?php

namespace Mushroom;

use RuntimeException;
use GuzzleHttp\Client;
use Mushroom\Pipeline;
use GuzzleHttp\ClientInterface;
use Mushroom\Contracts\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Mushroom\Contracts\Pipeline as PipelineInterface;

class Application
{
    private string $baseUrl;

    private string $targetUrl;

    public function __construct(
        private Container $container
    ) {
        $this->bootstrap();
    }

    public function baseUrl(): string
    {
        return $this->baseUrl;
    }

    public function targetUrl(): string
    {
        return $this->targetUrl;
    }

    protected function bootstrap()
    {
        $this->loadUrlsFromEnvironment();
        $this->registerBindings();
    }

    private function registerBindings()
    {
        $this->container->set(PipelineInterface::class, fn ($container) => $container->get(Pipeline::class));
        $this->container->set(Handler::class, fn ($container) => $container->get(RequestHandler::class));
        $this->container->set(ClientInterface::class, fn ($container) => $container->get(Client::class));
    }

    private function loadUrlsFromEnvironment()
    {
        $baseUrl = env('BASE_URL');
        $targetUrl = env('TARGET_URL');

        if (!is_string($baseUrl) || !is_string($targetUrl)) {
            throw new RuntimeException('BASE_URL and TARGET_URL enviroment variables must be set.', 1);
        }

        $this->baseUrl = $baseUrl;
        $this->targetUrl = $targetUrl;
    }

    /**
     * Run the application and send the response.
     *
     * @param Request|null  $request
     *
     * @return void
     */
    public function run($request = null): void
    {
        $request = $request ?? $this->parseIncomingRequest();
        $response = $this->dispatch($request);

        $response->send();
    }

    /**
     * Dispatch the incoming request.
     *
     * @param  SymfonyRequest|null  $request
     * @return Response
     */
    private function dispatch($request)
    {
        $handler = $this->container->get(Handler::class);

        return $handler->handle($request);
    }

    /**
     * Parse the incoming request and return the method and path info.
     *
     * @param  \Symfony\Component\HttpFoundation\Request|null  $request
     * @return array
     */
    protected function parseIncomingRequest()
    {
        Request::enableHttpMethodParameterOverride();

        $request = Request::createFromGlobals();
        $this->container->singleton(Request::class, fn () => $request);

        return $request;
    }
}
