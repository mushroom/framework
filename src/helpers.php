<?php

if (!function_exists('env')) {

    function env($key, $default = null)
    {
        $value = $_ENV[$key] ?? $default;

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (preg_match('/\A([\'"])(.*)\1\z/', $value, $matches)) {
            return $matches[2];
        }

        return $value;
    }
}

if (!function_exists('app')) {

    function app()
    {
        return resolve(\Mushroom\Application::class);
    }
}

if (!function_exists('resolve')) {

    function resolve($name)
    {
        return \Mushroom\Container::resolve($name);
    }
}
