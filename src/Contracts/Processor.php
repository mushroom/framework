<?php

namespace Mushroom\Contracts;

use Closure;
use Symfony\Component\HttpFoundation\Response;

interface Processor
{
    public function process(Response $response, Closure $next): Response;
}
